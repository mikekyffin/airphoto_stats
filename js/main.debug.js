﻿// file:            main.debug.js
// author(s):       mike kyffin
// build target:    main.js
//
// Copyright (c) 2012-2014 Maps, Data and Government Information Center (MaDGIC) at Trent University
// http://www.trentu.ca/library/madgic/
// Now featuring super-verbose commenting for internal use!

var app = app || {};
(function ($) {
    _config = {
        //"MENU": "handlers/menu.ashx",
        MENU: "http://" + ((document.location.host.match(/^albers|madgic3|localhost/i)) ? "albers.trentu.ca" : "mercator.trentu.ca") + "/resources/services/getServiceDefinitions.ashx",
        HOST: document.location.host.match(/^localhost|albers|madgic3/i) ? "ALBERS" : "MERCATOR",
        REHA_BOUND_AS_STRING: "{rings: [[[-8768221.2503999993, 5445343.7643000036], [-8783438.1776000001,5662030.5182000026],[-8684445.2515999991,5699109.9177000001],[-8615960.1580999997,5458181.9827999994],[-8768221.2503999993,5445343.7643000036]]] }"
    };
    app.init = function () {
        console.log("...initializing app");
        this.ui.workingMessage("Querying server: " + _config.HOST);
        this.data.init();
    };
    app.ui = {
        Data: {
            "services": [],
            "summary": {
                "count": 0,
                "rehacount": 0
            }
        },
        clearTable: function(table){
            $(table).remove();
        },
        showWorking: function(){
            // TODO?
        },
        hideWorking: function(){
            $(".opaque").fadeOut();
            this.workingMessage("");
        },
        workingMessage: function(msg){
            $(".message").html((msg) ? msg : "Working!");
        },
        Table: function (data, node, sortType, /*Boolean*/ isReha) {
            var mod = (isReha) ? "reha" : "";
            var table = $("<table>")
                .addClass("ap-table");
            data.services.sort(function (a, b) {
                var sorter = (sortType === "year") ? "year" : mod + sortType;
                return (a[sorter] > b[sorter]) ? 1 : ((b[sorter] > a[sorter]) ? -1 : 0);
            });
            var title = $("<tr>", {
                html: $("<td>", {
                    "colspan": 3,
                    "html": (isReha) ? "REHA services on " + _config.HOST : "All services on " + _config.HOST
                })
            }).appendTo(table);
            var header = $("<tr>")
                .addClass("ap-table-border-bottom")
                .appendTo(table);
            $("<td>", {
                html: "Year",
                "data-sort": "year",
                click: function (evt) {
                    $(node).empty();
                    new app.ui.Table(data, node, $(evt.target).data("sort"), isReha).render();
                }
            })
                .addClass("ap-table-10 ap-table-btn")
                .appendTo(header);
            $("<td>", {
                html: "Photos",
                "data-sort": "count",
                click: function (evt) {
                    $(node).empty();
                    new app.ui.Table(data, node, $(evt.target).data("sort"), isReha).render();
                }
            })
                .addClass("ap-table-10 ap-table-btn")
                .appendTo(header);
            $("<td>", {
                html: ""
            })
                .addClass("ap-table-80 ap-table-align-left")
                .appendTo(header);
            $.each(data.services, $.proxy(function (i, o) {
               var row = $("<tr>").appendTo(table);
               var year = $("<td>", {
                    html: o["year"],
                    style: ""
               })
                   .addClass("ap-table-10")
                   .appendTo(row);

                var count = $("<td>", {
                    html: o[mod + "count"],
                    style: ""
                })
                    .addClass("ap-table-10 ap-table-align-right")
                    .appendTo(row);
                var bar = $("<td>")
                    .addClass("ap-table-80 ap-table-align-left")
                    .appendTo(row);
                var barHolder = $("<div>").appendTo(bar);
                
                var value = ((o[mod + "count"] / data.summary[mod + "count"]) * 100);
                var theBar = $("<div>", {
                    html: (Math.round(value * Math.pow(10, 2)) / Math.pow(10, 2)) + "%"
                })
                    .addClass("ap-table-bar")
                    .css("width", value + "%")
                    .appendTo(barHolder);
            }, this));
            //
            var footer = $("<tr>").appendTo(table);
            $("<td>", {
                html: "TOTAL"
            })
                .addClass("ap-table-10")
                .appendTo(footer);
            $("<td>", {
                html: data.summary[mod + "count"]
            })
                .addClass("ap-table-10 ap-table-align-right")
                .appendTo(footer);
            var bar = $("<td>")
                    .addClass("ap-table-80 ap-table-align-left")
                    .appendTo(footer);
            var barHolder = $("<div>").appendTo(bar);            
            var value = 100;
            var theBar = $("<div>", {
                html: value + "%"
            })
                .addClass("ap-table-bar")
                .css("width", value + "%")
                .appendTo(barHolder);
            // everything
            return {
                domNode: table,
                render: function () {
                    node.append(this.domNode);
                },
                destroy: function () {
                    this.domNode.remove();
                }
            }
        },
        //
        comment: function (data, node) {
            $("<div>", {
                html: "<b>" + data.services.length + "</b> services reporting with <b>" + data.summary.count + "</b> air photos"
            })
                .addClass("top-comment")
                .appendTo(node);
            $("<hr>").appendTo(node);
        },
        //
        onDataError: function (err) {
            var errNode = $("<div>", {
                html: "<br/><br/>Error! Something went wrong."
            }).addClass("data-error");      
            if (err["statusText"]) {
                errNode.append($("<div>", {
                    html: err["statusText"] + ((err["status"]) ? " (" + err["status"] + ")" : "")
                }));
            }
            $(".opaque").html(errNode);
        }
    },
    app.data = {       
        init: function () {
            var all = this.getServices();
            all.done($.proxy(function (data) {
                // save length and give us a counter (n)
                var c = data.menu[0].items.length, n = 0;
                var msgString = "Found " + c + " services. Retrieving data...<br / >";
                app.ui.workingMessage(msgString);
                // iterate the services
                $.each(data.menu[0].items, $.proxy(function (i, o) {
                    app.ui.Data.services.push({
                        "year": parseInt(o.title.replace("y", "")),
                        "url": o.url,
                        "count": 0,
                        "rehacount": 0
                    });
                    // make another async call to the service to get the count
                    var xhr = this.getFullSummary(o);                  
                    xhr.done($.proxy(function (allData) {
                        msgString += ("<span class=\"message-year\">" + o["title"].replace("y", "") + "</span>");
                        app.ui.workingMessage(msgString);
                        // save the data coming back for the UI to use
                        $.grep(this.Data.services, function (v) {
                            return v.year == parseInt(o.title.replace("y", ""))
                        })[0]["count"] = allData.count;
                        this.Data.summary.count = this.Data.summary.count + allData.count;
                        // now get the data for the reha boundary - also async
                        var rxhr = app.data.getRehaSummary(o);
                        rxhr.done($.proxy(function (rData) {
                            // save the data coming back for the UI to use
                            $.grep(this.Data.services, function (v) {
                                return v.year == parseInt(o.title.replace("y", ""))
                            })[0]["rehacount"] = rData.count;
                            this.Data.summary.rehacount = this.Data.summary.rehacount + rData.count;
                            // up the counter
                            n++;
                            // get rid of the "loading screen" and show 
                            // build the report
                            if (n == c) {
                                this.hideWorking();
                                this.comment(this.Data, $("#top"));
                                var allTbl = new this.Table(this.Data, $("#all"), "year", false);
                                allTbl.render();
                                var rehaTbl = new this.Table(this.Data, $("#reha"), "year", true);
                                rehaTbl.render();
                            }
                        }, this));
                    }, app.ui));
                }, this));
            }, this));
            all.fail($.proxy(function (err) {
                this.onDataError(err);
            }, app.ui));
        },
        getFullSummary: function (item) {
            return this.request(item.url + "/0/query", {
                "f": "json",
                "where": "1=1",
                "returnCountOnly": true
            });
        },
        getRehaSummary: function(item) {
            return this.request(item.url + "/0/query", {
                f: "json",
                where: "1=1",
                returnCountOnly: true,
                geometryType: "esriGeometryPolygon",
                geometry: _config.REHA_BOUND_AS_STRING,
                inSR: "{ \"wkid\": 102100 }",
                spatialRel: "esriSpatialRelIntersects"
            });
        },
        getServices: function () {
            return this.request("handlers/proxy.ashx?" + _config.MENU, {
                dir: "airphoto"
            });
        },
        request: function (_url, _params) {
            return $.ajax({
                type: "GET",
                url: _url + "?",
                data: _params,
                dataType: "json"
            });
        }
    }
}(jQuery));